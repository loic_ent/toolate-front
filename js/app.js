var app = angular.module('toolate', []);

app.controller('InitController', ['$scope','$window','$http',InitController]);
app.controller('EventController', ['$scope','$window','$http', EventController]);
app.controller('UserController', ['$scope','$window','$http', UserController]);
app.controller('ListEventController', ['$scope','$window','$http', ListEventController]);

function InitController($scope,$window,$http){

    $window.init = function() {
    console.log("windowinit called");
    apisToLoad = 1; // must match number of calls to gapi.client.load()
    apiRoot = '//' + window.location.host + '/_ah/api';
    gapi.client.load('oauth2', 'v2', function(){
        console.log('loaded');});
    }

    function init() {
        var apisToLoad;
        var loadCallback = function() {
            if (--apisToLoad == 0) {
                signin(true, userAuthed);
            }
        };
    }

    function signin(mode, authorizeCallback) {
        gapi.auth.authorize({client_id: "658857847343-sjn3ht9p2vcorueap9s8m6jnfs5qvhl9.apps.googleusercontent.com",
            scope: "https://www.googleapis.com/auth/userinfo.email", immediate: mode},
            authorizeCallback);
    }

    function userAuthed() {
        var request =
        gapi.client.oauth2.userinfo.get().execute(function(resp) {
            if (!resp.code) {
                $scope.connect=true;
                $scope.user=resp;
                $scope.user.pref=[];
                $scope.user.events = [];
                $scope.$apply();

                //load user of our API
                console.log($scope.user.id);
                $http.get('https://toolate-maggle.appspot.com/_ah/api/userendpoint/v1/user/'+$scope.user.id).success(function(data, status, headers, config) {
                        console.log("user get done");
                           if(data.catsPreference.length!=0){
                             for(var i=0; i< data.catsPreference.length;i++){
                                $http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/'+data.catsPreference[i]).success(function(data, status, headers, config) {
                                    $scope.user.pref.push(data[0]);
                                });
                            }
                         }
                }).error(function(data, status, headers, config) {
                    // Simple POST request example (passing data) :
                    $http.post('https://toolate-maggle.appspot.com/_ah/api/userendpoint/v1/user', {id:$scope.user.id,catsPreference:[]}).
                    success(function(data, status, headers, config) {
                        console.log("create");
                    });
                });       

            }
        });



    }

    $scope.auth = function() {
        signin(false, userAuthed);
    };

    $scope.signout = function() {
        gapi.auth.setToken(null);
        $scope.connect=false;
        $scope.alreadyAdd = "";
        $scope.$apply();
        resp=null;
        console.log("deconnecte");
    }
}




function EventController($scope,$window,$http){

    //tab qui contient les catégories de bases 
    // culture, Sport, brocantes et balades
    var catsBases = ["p2_100729","p2_100730","p2_100731","p2_100732"];
    $scope.allCats = [];

    $scope.displayCats = function(){
        // pour tout le 1er lvl
        for(var i=0; i< catsBases.length;i++){// for catsBase
            $http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/'+catsBases[i]).success(function(data, status, headers, config) {
                $scope.allCats.push(data[0]);
            });
            $http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/'+catsBases[i]+'/children').success(function(data, status, headers, config) {
                    childs = data[0].children;
                    for(var c=0; c < childs.length;c++){
                        $scope.allCats.push(childs[c]);
                        $http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/'+childs[c].id+'/children').success(function(data, status, headers, config) {
                            for(var d=0; d < data[0].children.length;d++){
                            console.log(data[0].children[d]);
                            $scope.allCats.push(data[0].children[d]); 
                            }

                        });
                    }
                });
        }
    }

    $scope.isEmpty = function (obj) {
        for (var i in obj) if (obj.hasOwnProperty(i)) return false;
        return true;
    };
}


function UserController($scope,$window,$http){
    $scope.date = Date.now();
    $scope.addPref = function(cat){
        $scope.alreadyAdd ="";     
        for(var i=0; i < $scope.user.pref.length;i++){
            if($scope.user.pref[i].id == cat.id){
                $scope.alreadyAdd = "Catégorie déjà ajoutée";
            }
        }
        if ($scope.alreadyAdd == "") {
            $scope.user.pref.push(cat);
            $scope.alreadyAdd = "";
            console.log($scope.user.pref);
        }            
    }

    $scope.savePref = function(){
        $scope.alreadyAdd ="Patientez SVP";
        idCatsPref = [];
        for(var i=0; i < $scope.user.pref.length;i++){
            idCatsPref.push($scope.user.pref[i].id);
        }
        $http.put('https://toolate-maggle.appspot.com/_ah/api/userendpoint/v1/user', {id:$scope.user.id,catsPreference:idCatsPref}).success(function(data, status, headers, config) {
            console.log("datastore write");
            $scope.alreadyAdd ="Préférences sauvegardées";
        });
    }

    $scope.removePref = function(cat){
        $scope.user.pref.indexOf(cat);
        $scope.user.pref.splice($scope.user.pref.indexOf(cat), 1);
        console.log($scope.user.pref);
    }
}


function ListEventController($scope,$window,$http){

    $scope.displayEvent = function() {
        $scope.user.events = [];
        //liste des events dans le cats de user
        for(var i=0; i < $scope.user.pref.length;i++){
            $http.get('http://api.loire-atlantique.fr:80/opendata/1.0/event/summary?catIds='+$scope.user.pref[i].id).success(function(data, status, headers, config) {
                    if(data.data != null)
                        for(var j=0; j < data.data.length;j++){
                            $scope.user.events.push(data.data[j]);
                            console.log($scope.user.events);
                        }
            });
        }
    }
}

